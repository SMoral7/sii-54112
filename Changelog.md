 # Changelog.txt 
 # CREATED BY STELLA MORAL SALGUERO.
 > VERSIONES
 
 
 * ## V5.0 22/12/2020. FIN PRACTICA 5
 	+ ### Funciones de Cliente y Servidor modificadas. En el cliente se 			añade el nombre de la persona que va a jugar. 
 	+ ### Comunicación realizada mediante sockets.
 	+ ### Eliminación de tuberías.
 * ## V4.0 09/12/2020. FIN PRACTICA 4.
 	+ ### Funciones de Cliente y Servidor implementadas. Se añade la 		función de separar en diferentes pantallas en tiempo real.
 	+ ### Mundo.cpp , Mundo.h han sido sustituidas por 		 	MundoServidor.cpp, MundoCliente.cpp , MundoServidor.h y 		MundoCliente.h
 	+ ### MundoServidor ya no está controlado por un bot , solo 		MundoCliente.
 	+ ### MundoCliente y MundoServidor conectadas por tuberías e hilos.
 	+ ### CMakelists.txt actualizado para crear los ejecutables ./	cliente , ./logger , ./servidor y ./bot en ese orden.
 
 * ## V3.O  25/11/2020 . FIN PRACTICA 3. 
 	+ ### Bot añadido. Crea inteligencia artificial moviendo la raqueta 		en función de la posición de la esfera.
 	+ ### Logger añadido.
 	+ ### Función de marcador implementada.
 	+ ### Ganador disponible por pantalla.
 	+ ### Logger.cpp y bot.cpp conectadas por tuberías.
 	+ ### Funcionalidad extra añadida: cuando un jugador llega a tres 			puntos el juego termina.
 	+ ### CMakelists.txt actualizado para crear los 			ejecutables ./	logger , ./tenis, ./bot en ese orden.
 
* ## V2.0  22/10/2020 . FIN PRACTICA 2. 
 	+ ### Cambios en Esfera.cpp , Raqueta.cpp , Esfera.h y Raqueta.h
	+  ### Funcionalidad extra añadida: la esfera cambia su tamaño 		durante el juego.
 	+ ### Añadido movimientos a la esfera y a la raqueta. 
 	+ ### Añadidos nuevos comentarios. 
 	+ ### README.txt creado para contener las instrucciones del juego.
 	+ ### Changelog.txt actualizado.
 

 * ## V1.3  08/10/2020. FIN PRACTICA 1. 

 	+ ### Cabeceras y archivos cambiados. 
 	+ ### Nuevos comentarios añadidos.
 	+ ### Comentarios y etiquetas subidos a la plataforma Bitbucket.
 	+ ### Changelog.txt creado.

