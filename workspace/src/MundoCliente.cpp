// MundoCliente.cpp: implementation of the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////
#define N 100

#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <pthread.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* projection;

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo() //destructor
{

	char finjuego[N];
	sprintf(finjuego, "****** GAME OVER *****");
	write(pipe,finjuego,strlen(finjuego)+1); //escritura en la tuberia 
	close(pipe); //cierre tubería
	//close(coordenadas_pipe);
	//close(teclas_pipe);
	//unlink("/tmp/coordenadas_pipe");
	//unlink("/tmp/teclas_pipe");
	munmap(projection,sizeof(MemComp)); //desproyección
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[N];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		
		char cad[N]; //punto para jugador 2
		sprintf(cad, "JUGADOR2 +1 PUNTO\n Jugador1 : %d puntos\n Jugador2 : %d puntos \n",puntos1,puntos2);
		write(pipe,cad,strlen(cad)+1); //escritura en la tubería
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		char cad[N]; //punto para jugador 1
		sprintf(cad, "JUGADOR1 +1 PUNTO\n Jugador1 : %d puntos\n Jugador2 : %d puntos \n",puntos1,puntos2);
		write(pipe,cad,strlen(cad)+1); //escritura en la tubería
	}
	
	pMemComp->esfera=esfera; //para actualizar los datos de la proyeccion
	pMemComp->raqueta1=jugador1; //para actualizar los datos de la proyeccion
	
	switch(pMemComp->accion)
	{
	
	case 1: jugador1.velocidad.y=4; //velocidad hacia arriba
		break;
	case -1: jugador1.velocidad.y=-4; //velocidad hacia abajo
		break;
	case 0: break; //no hace nada, pero se añade para cubrir todos los casos
	}

// código para imprimir el ganador. Máquina de estados

int aux1=0; //estado actual
int aux2=0; //estado anterior
char cad[N];

if(puntos1==3)
{
	aux1=1;
	if(aux1!=aux2) {
	sprintf(cad, "EL GANADOR ES EL JUGADOR1");
	write(pipe,cad,strlen(cad)+1);
	}
	
	aux2=aux1;
	exit(0);

}

if(puntos2==3)
{	
	aux1=1;
	if(aux1!=aux2) {
	sprintf(cad, "EL GANADOR ES EL JUGADOR2");
	write(pipe,cad,strlen(cad)+1);
	}
	
	aux2=aux1;
	exit(0);

}

  char cadena [200];
  
 
  s_conexion.Receive(cadena, 200);
  sscanf(cad, " %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y , &jugador1.x1 , &jugador1.y1 , &jugador1.x2, &jugador1.y2 , &jugador2.x1 , &jugador2.y1 , &jugador2.x2 , &jugador2.y2 , &puntos1 , &puntos2);    
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[]="0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w");break;
	case 'l':sprintf(tecla,"l");break;
	case 'o':sprintf(tecla,"o");break;

	}
	s_conexion.Send(tecla, sizeof(tecla));
	
	//char c_keys[5];
	//sprintf(c_keys, "%c" , key);
	//write(teclas_pipe,c_keys, sizeof(c_keys)+1);
}

void CMundo::Init()
{
	char IP[]="127.0.0.1";
	char nombre[100];
	printf(" NOMBRE: ");
	scanf("%s" , nombre);
	//faltaba esto 
  	s_conexion.Connect("127.0.0.1",8000);
	s_conexion.Send(nombre , sizeof(nombre));
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//implementación de logger
	
	pipe=open("/tmp/logger_fifo",O_WRONLY); //modo solo escritura
	
	//implementación del bot
	
	int fd=open("/tmp/datos_bot.txt",O_RDWR|O_CREAT|O_TRUNC,0777); //creación del descriptor de ficheros con todos los permisos.
	if(fd<0) {
	perror("ERROR");
	exit(1);
	}
	
	write(fd, &MemComp,sizeof(MemComp)); //escritura del fichero
	projection=(char*)mmap(NULL,sizeof(MemComp),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	if(projection==MAP_FAILED) {
	perror("ERROR");
	exit(1);
	}
	close(fd); //cierre del descriptor de fichero
	pMemComp=(DatosMemCompartida*)projection;
	pMemComp->accion=0; //acción para la raqueta 
	
	//añadir las dos tuberías
	
	//mkfifo("/tmp/coordenadas_pipe",0777); // tubería para enviar coordenadas
	//coordenadas_pipe=open("/tmp/coordenadas_pipe",O_RDONLY); //SOLO LECUTRA
	
	//mkfifo("/tmp/teclas_pipe",077); // tubería para pasar teclas
	//teclas_pipe=open("/tmp/teclas_pipe",O_WRONLY); // SOLO ESCRITURA
	
	

}
