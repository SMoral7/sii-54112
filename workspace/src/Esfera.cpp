// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	radio_min=0.2f;
	radio_max=0.6f;
	pulso=0.05;
}

Esfera::~Esfera()
{

}

void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+ velocidad*t;
	//if(radio<radio_min)
	//pulso=-pulso;
	//if(radio>radio_max)
	//pulso=-pulso;
	//radio+=pulso*t;
}





