// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#define N 100

#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include<pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos( void *d) {

	CMundo *p=(CMundo*)d;
	p -> RecibeComandosJugador();
	
	}
	
void CMundo::RecibeComandosJugador() {

	 while(1){
	 
	 usleep(10);
	 char cad[100];
	 s_comunicacion.Receive(cad, sizeof(cad));
	 unsigned char key;
	 sscanf(cad, "%c" , &key);
	 if (key == 's') jugador1.velocidad.y= -4;
	 if (key == 'w') jugador1.velocidad.y= 4;
	 if (key == 'l') jugador2.velocidad.y= -4;
	 if (key == 'o') jugador2.velocidad.y= 4;
	 }
}

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo() //destructor
{

	char finjuego[N];
	sprintf(finjuego, "****** GAME OVER *****");
	write(pipe,finjuego,strlen(finjuego)+1); //escritura en la tuberia 
	close(pipe); //cierre tubería
	//close(coordenadas_pipe); //cierre tubería servidor->cliente
	//close(teclas_pipe); //cierre tubería cliente-> servidor
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[N];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		
		char cad[N]; //punto para jugador 2
		sprintf(cad, "JUGADOR2 +1 PUNTO\n Jugador1 : %d puntos\n Jugador2 : %d puntos \n",puntos1,puntos2);
		write(pipe,cad,strlen(cad)+1); //escritura en la tubería
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		char cad[N]; //punto para jugador 1
		sprintf(cad, "JUGADOR1 +1 PUNTO\n Jugador1 : %d puntos\n Jugador2 : %d puntos \n",puntos1,puntos2);
		write(pipe,cad,strlen(cad)+1); //escritura en la tubería
	}
	
	
	//pMemComp->esfera=esfera; //para actualizar los datos de la proyeccion
	//pMemComp->raqueta1=jugador1; //para actualizar los datos de la proyeccion
	
	//switch(pMemComp->accion)
	//{
	
	//case 1: jugador1.velocidad.y=4; //velocidad hacia arriba
		//break;
	//case -1: jugador1.velocidad.y=-4; //velocidad hacia abajo
		//break;
	//case 0: break; //no hace nada, pero se añade para cubrir todos los casos
	//}

// código para imprimir el ganador. Máquina de estados

int aux1=0; //estado actual
int aux2=0; //estado anterior
char cad[N];

if(puntos1==3)
{
	aux1=1;
	if(aux1!=aux2) {
	sprintf(cad, "EL GANADOR ES EL JUGADOR1");
	write(pipe,cad,strlen(cad)+1);
	}
	
	aux2=aux1;
	exit(0);

}

if(puntos2==3)
{	
	aux1=1;
	if(aux1!=aux2) {
	sprintf(cad, "EL GANADOR ES EL JUGADOR2");
	write(pipe,cad,strlen(cad)+1);
	}
	
	aux2=aux1;
	exit(0);

}

char cadena [200];
  
  sprintf(cad, " %f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y , jugador1.x1 , jugador1.y1 , jugador1.x2, jugador1.y2 , jugador2.x1 , jugador2.y1 , jugador2.x2 , jugador2.y2 , puntos1 , puntos2); 
 	s_comunicacion.Send(cadena, sizeof(cadena));
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	char IP[]="127.0.01";
	if(s_conexion.InitServer(IP,8000)<0){
	
	perror("ERROR AL ABRIR EL SERVIDOR .\n");
	exit(1);
	}
	s_comunicacion=s_conexion.Accept();
	char nombre[N];
	s_comunicacion.Receive(nombre, sizeof(nombre));
	
	printf( " %s se ha unido a la sesion" , nombre);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//implementación de logger
	
	pipe=open("/tmp/logger_fifo",O_WRONLY); //modo solo escritura
	
	// implementar cliente-servidor
	
	
	//coordenadas_pipe=open("/tmp/coordenadas_pipe",O_WRONLY); //SOLO ESCRITURA
	pthread_create(&thid1,NULL,hilo_comandos,this); //creación del hilo
	//teclas_pipe=open("/tmp/teclas_pipe",O_RDONLY); // SOLO LECTURA
}
